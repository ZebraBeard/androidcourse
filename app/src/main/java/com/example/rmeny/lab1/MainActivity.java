package com.example.rmeny.lab1;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    Button button2;
    EditText editText;
    Button activity2;
    Button activity3;
    TextView result;

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        editText = (EditText) findViewById(R.id.text);
        button2 = (Button) findViewById(R.id.button2);
        result = (TextView) findViewById(R.id.result_text);
        button2.setOnClickListener(this);
        activity2 = (Button) findViewById(R.id.activity2);
        activity3 = (Button) findViewById(R.id.activity3);
        activity2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, SecondActivity.class);
                startActivityForResult(intent, 1);
            }
        });
        activity3.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                startActivity(new Intent(Intent.ACTION_VIEW).setData(Uri.parse("http://www.google.com")));
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (data == null) {
            return;
        }
        Double resultFromActivity = data.getDoubleExtra("result",0);
        result.setText("Result: "+ resultFromActivity.toString());
    }

    @Override
    public void onClick(View v) {
        Intent intent = new Intent(this, FirstActivity.class);
        intent.putExtra("textField", editText.getText().toString());
        startActivity(intent);
    }
}
