package com.example.rmeny.lab1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class SecondActivity extends AppCompatActivity {

    TextView numberField;
    Button log;
    Button exp;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);
        numberField = (TextView) findViewById(R.id.numberField);
        log = (Button) findViewById(R.id.log);
        exp = (Button) findViewById(R.id.exp);
        log.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.putExtra("result", Math.log(Double.parseDouble(numberField.getText().toString())));
                setResult(RESULT_OK, intent);
                finish();
            }
        });
        exp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.putExtra("result", Math.exp(Double.parseDouble(numberField.getText().toString())));
                setResult(RESULT_OK, intent);
                finish();
            }
        });
    }
}
